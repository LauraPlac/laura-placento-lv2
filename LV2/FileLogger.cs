﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{

    //Z4.
    class FileLogger : ILogger
    {
        public String FilePath { get; set; }
        public FileLogger(String filePath)
        {
            this.FilePath = filePath;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.FilePath, true))
            {
                writer.WriteLine(message);
            }

        }

    }
}
