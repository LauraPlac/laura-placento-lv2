﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Z1.
            //DiceRoller diceRoller = new DiceRoller();
            //for(int i = 0; i < 20; i++)
            //{
            //    diceRoller.InsertDie(new Die(6));

            //}
            //diceRoller.RollAllDice();
            //PrintRollingResults(diceRoller);


            //Z2.
            //Random Generator = new Random();
            //DiceRoller diceRoller = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    diceRoller.InsertDie(new Die(6, Generator));

            //}
            //diceRoller.RollAllDice();
            //PrintRollingResults(diceRoller);


            //Z3.
            //DiceRoller diceRoller = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    diceRoller.InsertDie(new Die(6));

            //}
            //diceRoller.RollAllDice();
            //PrintRollingResults(diceRoller);



            //Z4.
            DiceRoller diceRoller = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));

            }
            diceRoller.RollAllDice();
            diceRoller.LogRollingResults();

            FileLogger fileLogger = new FileLogger("Logger.txt");
            diceRoller.SetLogger(fileLogger);
            diceRoller.LogRollingResults();



        }

        static void PrintRollingResults(DiceRoller diceRoller)
        {
            IList<int> RollingResults = diceRoller.GetRollingResults();
            foreach (int RollingResult in RollingResults)
            {
                Console.WriteLine(RollingResult);
            }
        }
    }


}
