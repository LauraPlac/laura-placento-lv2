﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace LV2
{
    //Z1.
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = new Random();
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}


    //Z2.
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides, Random random)
    //    {

    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = random;
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}


    //Z3.
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {

            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
            
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }


}
